#!/usr/bin/env python3.12

import sys
import base64
try:
    import argparse
    from argparse import RawTextHelpFormatter
except:
    sys.exit("Please install missing library: python3.12 -m pip install --break-system-packages argparse")

parser = argparse.ArgumentParser(description="Converts an html website to base64",
                                 formatter_class=RawTextHelpFormatter,
                                 epilog="Examples:\n"
                                        "./site_base64.py -i site.html -o obfuscated.html")
parser.add_argument("--input", "-i", help="url", required=True)
parser.add_argument("--output", "-o", help="url", required=True)
args = parser.parse_args()

with open(args.input, "r", encoding="utf-8") as file:
    content = file.read()
    base64_content = base64.b64encode(content.encode("utf-8")).decode()

obfuscated_html = f'''<script>
  document.write(atob('{base64_content}'));
</script>'''

with open(args.output, 'w') as file:
    file.write(obfuscated_html)
